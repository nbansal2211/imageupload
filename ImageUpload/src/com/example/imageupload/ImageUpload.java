package com.example.imageupload;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageUpload extends Activity implements OnClickListener,
		AsyncTaskInterface {

	Button pickImage, go;
	Uri outputFileUri;
	ImageView image;
	private static final int PICK_IMAGE_CODE = 11;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_upload);
		findViewById(R.id.pickImage).setOnClickListener(this);
		findViewById(R.id.go).setOnClickListener(this);
		image = (ImageView) findViewById(R.id.imageToUpload);
		findViewById(R.id.go).setClickable(false);
		String uri = getPrefsURI();
		if (uri != null) {
			outputFileUri = Uri.parse(uri);
			image.setImageURI(outputFileUri);
			findViewById(R.id.go).setClickable(true);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.go:
			if(outputFileUri == null){
				Toast.makeText(this, "Please pick an image to upload", Toast.LENGTH_LONG).show();
				return;
			}
			EditText text = (EditText) findViewById(R.id.url);
			String url = text.getText().toString().trim();
			if("".equals(url)){
				Toast.makeText(this, "URL cannot be empty", Toast.LENGTH_LONG).show();
				return;
			}
			StringBuilder builder = new StringBuilder();
			builder.append("http://");
			builder.append(url);
			startImageUpload(builder.toString());
			break;
		case R.id.pickImage:
			startCamera();
			break;
		}
	}
	
	private void startImageUpload(String url){
		
		DownloadTask task = new DownloadTask(this, url, getJsonFromImageURI(outputFileUri), this);
		task.execute();
	}

	public void startCamera() {
		File photo = null;
		String FILE_NAME = "pic_image.jpg";
		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		photo = new File(getCacheDir(), FILE_NAME);

		if (photo != null) {

			try {
				startActivityForResult(intent, PICK_IMAGE_CODE);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private String getPrefsURI() {

		SharedPreferences prefs = getSharedPreferences("ImagePrefs",
				MODE_PRIVATE);
		return prefs.getString("URI", null);

	}

	private void setPrefs(Uri uri) {
		SharedPreferences prefs = getSharedPreferences("ImagePrefs",
				MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putString("URI", uri.toString());
		editor.commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == PICK_IMAGE_CODE) {
				outputFileUri = data.getData();
				image.setImageURI(outputFileUri);
			}
		}
	}

	public JSONObject getJsonFromImageURI(Uri imgUri) {

		try {
			JSONObject jObMain = new JSONObject();
			if (imgUri != null) {
				ImageToStringConverter converter = new ImageToStringConverter();
				String imageString = converter.getImageBinaryJsonString(imgUri);
				if (imageString != null) {
					jObMain.put("img", imageString);
					return jObMain;
				}

			}

		} catch (JSONException e) {
			Log.e("JSON Exception AT", "Util. LastKnownLoc Json");
		}
		return null;
	}

	@Override
	public void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		String response = (String) result;

		Toast.makeText(ImageUpload.this, "Response Received is: " + response,
				Toast.LENGTH_LONG).show();
	}

}
