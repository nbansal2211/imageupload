package com.example.imageupload;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class DownloadTask extends AsyncTask {

	AsyncTaskInterface ref;
	Context ctx;
	String url;
	JSONObject jsonObj;
	public DownloadTask(Context ctx, String url, JSONObject jsonObj, AsyncTaskInterface ref){
		this.ref = ref;
		this.ctx = ctx;
		this.url = url;
		this.jsonObj = jsonObj;
	}
	@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		ServerConnectivity server = new ServerConnectivity();
		String response = server.makeRequestAndUpLoadJSON(url, jsonObj);
		return response;
	}
	
	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		ref.onPostExecute(result);
	}

}
