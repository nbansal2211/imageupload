package com.example.imageupload;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.apns.service.util.BASE64Encoder;

public class ImageToStringConverter {

	public String getImageBinaryJsonString(Uri imgUri) {
		// TODO Auto-generated method stub
		Bitmap photo = getPreview(imgUri);
		if (photo == null) {
			return null;
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] imageBytes = stream.toByteArray();
		BASE64Encoder encoder = new BASE64Encoder();
		String imageString = encoder.encode(imageBytes);
		return imageString;
	}

	private Bitmap getPreview(Uri uri) {
		File image = new File(uri.getPath());
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(image.getPath(), bounds);
		if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
			return null;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;
		return BitmapFactory.decodeFile(image.getPath(), opts);
	}

}
