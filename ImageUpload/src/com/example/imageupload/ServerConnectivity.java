package com.example.imageupload;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

public class ServerConnectivity {

	public String makeRequestAndUpLoadJSON(String url,
			final JSONObject submitedTestjson)
	// throws Exception
	{
		try {
			Log.e("URL to send is:", url);
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpost = new HttpPost(url);
			StringEntity se = null;

			Log.e("Submitted JSON IS:", submitedTestjson.toString());
			se = new StringEntity(submitedTestjson.toString());
			httpost.setEntity(se);
			httpost.setHeader("Accept", "application/json");
			httpost.setHeader("Content-type", "application/json");
			ResponseHandler responseHandler = new BasicResponseHandler();
			String response = "!!!!!";
			response = httpclient.execute(httpost, responseHandler);
			Log.e("Response Returned Is:", response);
			return response;
		} catch (Exception e) {
			Log.e("Exception Occurs", "Server Connectivity Class");
			return null;
		}
	}

}
